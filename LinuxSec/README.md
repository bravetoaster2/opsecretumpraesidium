# Linux Hardening Measures
## Hardening guide for debian-based linux distributions
### Initial Install - DebSet
- Run this script to update and install security applications (check script for what you desire)

### Kernel Hardening - sysctl.conf
- Replace the file in /etc/sysctl.conf 

### SSH Hardening - ssh_conf
- Replace the file in /etc/ssh/ssh_conf
    - May be sshd_conf on certain distros

### Security on Boot - startupscript
- Run chmod +x ./startupscript
    - Optionally set up a cron job to automatically open on login