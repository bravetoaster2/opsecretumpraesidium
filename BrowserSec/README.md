# Browser Hardening
## Instead of hardening firefox extensively for privacy, the TOR Browser has active development and anti-fingerprinting. To make this browser a daily driver, I recommend removing the tor proxy - aka using TOR Browser for clearnet only.
### Browser Configuration
- Type about:config in the search bar
- Change the following settings:
    - extensions.torbutton.use_nontor_proxy - true
    - extensions.torlauncher.prompt_at_startup - false
    - extensions.torlauncher.start_tor - false
    - network.proxy.socks_remote_dns - false
    - network.proxy.type - 0

### Disable Addons that reset settings
- Go to Settings | Add-ons
- Disable Tor Launcher

#### Congrats, you have an isolated anti-fingerprinting browser without the hassle of hours of research and configuration.