- [Android Security & Privacy Measures](#android-security---privacy-measures)
    + [Place limitations on Google Applications (if present)](#place-limitations-on-google-applications--if-present-)
    + [Install Open-Source, Privacy friendly applications](#install-open-source--privacy-friendly-applications)
    + [Sandbox Applications](#sandbox-applications)
    + [Permissions Audit](#permissions-audit)
    + [Software Microphone Protection](#software-microphone-protection)
    + [E2E Messengers](#e2e-messengers)
    + [Network Traffic](#network-traffic)
    + [System Settings](#system-settings)
    + [Firewall / Packet Filtering](#firewall---packet-filtering)
    + [Malware Scanning](#malware-scanning)
    + [Remove Bloat/Spyware](#remove-bloat-spyware)
    + [Alter GPS Callbacks](#alter-gps-callbacks)

<small><i><a href='http://ecotrust-canada.github.io/markdown-toc/'>Table of contents generated with markdown-toc</a></i></small>

# Android Security & Privacy Measures

##### The implementations on this guide that you choose to use will differ based on your threat model and the ROM you are running
##### If a custom ROM such as Lineage/Graphene or other variants are flashed, it is advisable to not flash GApps
##### It should be noted that rooting your device can reduce security overall.
##### For all intents, if security hardening is the ultimate goal, acquire an old Pixel and flash GrapheneOS.
- If you are running GrapheneOS, I personally advise to not root the device. 
- If you are on Lineage/Stock, rooting can provide a medley of privacy oriented features.

### Place limitations on Google Applications (if present)
- If you are using Gapps, disable any unnecessary services by holding down the application icon
- If uninstall is not an option, click disable and remove permissions
    - Open Google Play Store (assuming you don't choose to disable it)
    - Settings:
        - Auto-update apps - Don't auto-update apps
    - If you are on stock ROM, replace Gboard with AnySoftKeyboard or Simple Keyboard (if available)
    - If you keep gboard, settings to optimize privacy will be listed in System Settings instructions

### Install Open-Source, Privacy friendly applications
##### Install the following applications:
- F-Droid from https://f-droid.org/FDroid.apk
    - On F-Droid:
        - Shelter (Sandbox/isolation)
        - AFWall+ (firewall) (root)
        - PilferShush (Mic blocker) 
        - Scrambled Exif (photo metadata remover)
        - Simple Contacts (Disable Google Contacts)
        - Simple Gallery (Disable Google Gallery)
        - Simple Calendar (Disable Google Calendar)
        - OsmAnd+ (Disable Google Maps)
        - AdAway
        - Aurora Playstore
        - If on Stock ROM, search for AnysoftKeyboard or Simple Keyboard to replace Gboard
    - On Aurora Play Store (Sign in with anonymous generated account from Aurora)
        - App Ops (permissions audit) (root)
        - Signal or Wire (encrypted communications)
        - Proton/Nord/Mullvad (or any other premium VPN)
        - WARNING: (See issue regarding Xposed)
            - Xposed modules & xprivacylua (root) - Protects IMEI and other identifiers from applications
      
### Sandbox Applications
###### Do not put much faith into the sandbox. Shelter/Island can be circumvented
- Open the shelter app 
- Clone all applications suspected of embedded tracking to the Shelter (Gapp services may break)
- Delete those cloned applications in personal profile

### Permissions Audit
- Open App Ops
    - Audit application permissions thoroughly
    - Set every application to strict
    - Place priority on limiting background activity & clipboard accessibility

### Software Microphone Protection
- PilferShush
    - Set to passive mode
    - If accessible audio jack, hardware microphone blockers are the better solution

### E2E Messengers
- Signal
    - Set to default messenger if signal
    - Use a VoIP (Gvoice or mysudo is optional) 
        - Google Voice will not work without GApps - A workaround is to link your baseband prepaid number to GVoice if it must be used
        - Calls and messages will be forwarded to your number
- Wire/Riot
    - VoIP numbers are unnecessary

### Network Traffic
- ProtonVPN (or any other selected VPN)
    - Start with Android - On
    - DNS Leak - On
        - If DNS leak protection is not an option provided/supported by VPN, look into DNS over HTTPS (DoH) options
        - Options include: Blokada & DNSCrypt
 
### System Settings
- Connected Devices
- Connection Preferences
    - Disable NFC
    - Turn Printing off
- Security & Location
    - Lock Screen Preferences - Hide sensitive content
    - Location - Off
    - Show passwords - Off
    - Device Admin Apps - Remove "Find My Device"
- Accessibility
    - Volume key shortcut - Off
    - Remove animations
- System
    - Languages & input
    - Tap Virtual Keyboard
- If still using Gboard against my advice
    - Turn off all text correction, dictionary, and predictive search
        - Advanced
            - Share usage statistics - Off
            - Backup - Off
            - Turn on Developer options
                - System > Tap Build Number 5 times
                - Inside Developer options
                    - OEM Unlocking - off (should be off by default)
                    - USB Debugging - Off
                    - Revoke USB debugging authorizations
                    - Turn off Developer options
                    
    - Apps & Notifications
        - Select Individual applications that do not require data
        - Select Data usage
            - Disable the following:
                - Wi-Fi data
                - Cellular data
                - Background data
                - VPN data

### Firewall / Packet Filtering
###### For the rooted user, I recommend the use of AFWall on both personal and work profile.

- Open AFWall | Preferences
- Logging
    - Turn on Log Service
- Security
    - Enable protection (set passcode/pattern)
- Rules / Connectivity
    - Active rules - Check
    - LAN control - Check
    - VPN control - Check (This must be selected so you can audit which applications can reach the internet over VPN connections)
- System Services/Frameworks that you can filter without error:
    - Android Debug Bridge
    - Digital Rights Management service
    - GPS
    - Linux Kernel
    - Linux shell
    - Android Accessibility Suite
    - Android Setup
    - Android System Webview
    - Bluetooth
    - Calculator
    - Calendar
    - Calendar Storage
    - Camera
    - Captive PortalLogin
    - Clock
    - Contacts
    - Intent Filter Verification Service
    - Market Feedback Agent
    - Mobile Installer 
    - MusicFX
    - Nfc Service
    - Photos
    - Print Service
    - ProxyHandler
    - Sprint OMA-DM
    - System UI
- These services are all being packet filtered and logged.

### Malware Scanning
- Anti-Viruses can prove valuable in the event you run into a malicious APK or other malware variants
- Trust must be placed in AVs - resulting in a lack of privacy
    - Download Malwarebytes (or any other variant)
    - Allow the AV to update its signatures
        - Disable data from Application Settings & AFWall+(if rooted)
        - Scan your system
        - Remove the application

### Remove Bloat/Spyware                    
###### Optional: Remove the T, X, OK Google Enrollment with a system application remover. 
- Warning: This could result in your system losing functionality and ultimately crashing. Proceed with caution and make a backup (Titanium is a backup option)

### Alter GPS Callbacks
###### Remove callback to Goog's A-GPS SUPL servers (Device makes GPS requests with IMEI to these servers)
- Download termux (or other terminal application) | cd /system/etc/ | pkg install nano | nano gps.conf
    - Replace every mention of Goog's GPS SUPL servers with any of the following (more research needed on which are more private)
        - supl.sonyericsson.com
        - supl.vodafone.com
        - agpss.orange.fr
        - agps.supl.telstra.com